/*
 *     Kotlin Trashguy Library
 *     Copyright (C) 2021 Hackintosh Five
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License and General Lesser Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.hack5.trashguy

import kotlinx.benchmark.Benchmark
import kotlinx.benchmark.Scope
import kotlinx.benchmark.State
import kotlinx.benchmark.TearDown

@Suppress("unused")
open class TrashguyBenchmark {
    @State(Scope.Benchmark)
    open class TrashguyState {
        val plaintext = "a".repeat(100)
        val plaintextAsMultiChar = plaintext.map { charArrayOf(it).joinToString("") }
        val emoji = listOf(
                CustomLengthString(1, "👰"),
                CustomLengthString(2, "🧑‍🦰"),
                "abc",
                "hello",
                "t", "e", "s", "t", "i", "n", "g", "1", "2", "3", "4"
        )

        @TearDown
        fun tearDown() {
        }
    }

    @Benchmark
    open fun plaintext(state: TrashguyState): List<String> {
        val tg = Trashguy(state.plaintext, "🗑", "<(^_^ <)", "(> ^_^)>", " ", 2)
        return tg.copy()
    }

    //@Benchmark
    open fun emoji(state: TrashguyState): List<String> {
        val tg = Trashguy(state.emoji, "🗑", "<(^_^ <)", "(> ^_^)>", " ", 2)
        return tg.copy()
    }

    //@Benchmark
    open fun plaintextAsMultiChar(state: TrashguyState): List<String> {
        val tg = Trashguy(state.plaintextAsMultiChar, "🗑", "<(^_^ <)", "(> ^_^)>", " ", 2)
        return tg.copy()
    }
}

inline fun Trashguy.copy(): List<String> {
    return map { buildString { it.toCharArray() } }
}