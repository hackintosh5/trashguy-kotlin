/*
 *     Kotlin Trashguy Library
 *     Copyright (C) 2021 Hackintosh Five
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License and General Lesser Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.hack5.trashguy

import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@ExperimentalTime
fun main() {
    while (true)
        println(measure("a".repeat(15)))
}

@ExperimentalTime
fun measure(text: String) = measureTime {
    for (i in 0 until 5000) {
        Trashguy(text, "🗑", "<(^_^ <)", "(> ^_^)>", " ", 2).toList()
    }
}

@ExperimentalTime
fun measure(text: List<CharSequence>) = measureTime {
    for (i in 0 until 5000) {
        Trashguy(text, "🗑", "<(^_^ <)", "(> ^_^)>", " ", 2).toList()
    }
}