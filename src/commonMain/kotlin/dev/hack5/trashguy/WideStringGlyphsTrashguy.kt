/*
 *     Kotlin Trashguy Library
 *     Copyright (C) 2021 Hackintosh Five
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License and General Lesser Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.hack5.trashguy

internal class WideStringGlyphsTrashguy(
        val trashItems: List<CharSequence>,
        canGlyph: String,
        leftGlyph: String,
        rightGlyph: String,
        spaceGlyph: String,
        spacing: Int,
        trashSpaces: Boolean
) : Trashguy(canGlyph, leftGlyph, rightGlyph, spaceGlyph, spacing) {
    private val multiCharGlyphsTable: IntArray

    // in fact, we will need less than trashItems.size + 1 if we are ignoring spaces
    private val multiCharGlyphsITable: IntArray
    private val multiCharGlyphsMovedTable: IntArray

    override var size: Int = 0
        private set

    init {
        var i = 0
        var moved = 0
        var base: Int
        var count: Int
        var length = 0
        for (glyph in trashItems) {
            if (trashSpaces || glyph.isNotBlank())
                length += ((spacing + i) * 2 + 3)
            i += if (glyph is CustomLengthString) glyph.glyphLength else glyph.length
        }
        i = 0
        multiCharGlyphsITable = IntArray(length + 1)
        multiCharGlyphsMovedTable = IntArray(length + 1)
        multiCharGlyphsTable = IntArray(trashItems.size + 2)
        for (glyph in trashItems) {
            count = ((spacing + i) * 2 + 3)
            if (trashSpaces || glyph.isNotBlank()) {
                base = calcIndex(i + 1, spacing, spacingIndex)
                (size until size + count).zip(base until base + count).forEach {
                    multiCharGlyphsITable[it.first] = it.second
                    multiCharGlyphsMovedTable[it.first] = moved
                }
                size += count
            }
            multiCharGlyphsTable[moved] = i
            i += if (glyph is CustomLengthString) glyph.glyphLength else glyph.length
            moved++
        }
        require(size == length) { "$size != $length" }
        multiCharGlyphsMovedTable[length] = moved
        size++
    }

    override fun get(index: Int): String {
        val movedOverride = multiCharGlyphsMovedTable[index]
        val i = multiCharGlyphsITable[index]
        val moved = calcMoved(i, spacing, spacingIndex)
        val offset = calcIndex(moved, spacing, spacingIndex)
        val max = calcIndex(moved + 1, spacing, spacingIndex)
        val turningPoint = calcTurningPoint(offset, max)
        return if (i < turningPoint)
            getRight(i - offset, movedOverride)
        else
            getLeft(max - i - 1, movedOverride)
    }

    private fun getLeft(pos: Int, moved: Int) = buildString {
        append(canGlyph)
        if (pos > 0) {
            append(spaceGlyph.repeat(pos - 1))
            append(trashItems[moved])
            append(leftGlyph)
            append(spaceGlyph.repeat(multiCharGlyphsTable[moved] - pos + spacing + 1))
        } else {
            append(leftGlyph)
            append(spaceGlyph.repeat(multiCharGlyphsTable[moved + 1] + spacing))
        }
        append(trashItems.subList(moved + 1, trashItems.size).joinToString(""))
    }

    private fun getRight(pos: Int, moved: Int) = buildString {
        append(canGlyph)
        append(spaceGlyph.repeat(pos))
        append(rightGlyph)
        append(spaceGlyph.repeat(multiCharGlyphsTable[moved] + spacing - pos))
        append(trashItems.subList(moved, trashItems.size).joinToString(""))
    }
}