/*
 *     Kotlin Trashguy Library
 *     Copyright (C) 2021 Hackintosh Five
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License and General Lesser Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.hack5.trashguy

internal class SingleCharGlyphsTrashguy(
        val trashItems: CharSequence,
        canGlyph: String,
        leftGlyph: String,
        rightGlyph: String,
        spaceGlyph: String,
        spacing: Int,
) : Trashguy(canGlyph, leftGlyph, rightGlyph, spaceGlyph, spacing) {
    private val lineLengthLeft: Int
    private val lineLengthRight: Int

    init {
        val lineLength = trashItems.length + (spacing * spaceGlyph.length) + canGlyph.length
        lineLengthLeft = lineLength + leftGlyph.length
        lineLengthRight = lineLength + rightGlyph.length
    }

    override val size = calcIndex(trashItems.length + 1, spacing, spacingIndex) + 1

    override fun get(index: Int): String {
        val moved = calcMoved(index, spacing, spacingIndex)
        val offset = calcIndex(moved, spacing, spacingIndex)
        val max = calcIndex(moved + 1, spacing, spacingIndex)
        val turningPoint = calcTurningPoint(offset, max)
        return if (index < turningPoint)
            getRight(index - offset, moved - 1)
        else
            getLeft(max - index - 1, moved - 1)
    }

    private fun getLeft(pos: Int, moved: Int) = buildString(lineLengthLeft) {
        append(canGlyph)
        if (pos > 0) {
            append(spaceGlyph.repeat(pos - 1))
            append(trashItems[moved])
            append(leftGlyph)
            append(spaceGlyph.repeat(moved - pos + spacing + 1))
        } else {
            append(leftGlyph)
            append(spaceGlyph.repeat(moved + spacing + 1))
        }
        append(trashItems.substring(moved + 1))
    }

    private fun getRight(pos: Int, moved: Int) = buildString(lineLengthRight) {
        append(canGlyph)
        append(spaceGlyph.repeat(pos))
        append(rightGlyph)
        append(spaceGlyph.repeat(moved + spacing - pos))
        append(trashItems.substring(moved))
    }
}