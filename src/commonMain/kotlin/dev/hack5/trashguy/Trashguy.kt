/*
 *     Kotlin Trashguy Library
 *     Copyright (C) 2021 Hackintosh Five
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License and General Lesser Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.hack5.trashguy

import kotlin.math.sqrt

abstract class Trashguy protected constructor(
        val canGlyph: String,
        val leftGlyph: String,
        val rightGlyph: String,
        val spaceGlyph: String,
        val spacing: Int,
) : AbstractList<String>() {
    init {
        require(spacing >= 0) { "Spacing must be greater than or equal to zero" }
    }

    protected val spacingIndex = (spacing + 1) * (spacing + 1)
}

/**
 * Create a new single-char glyphs Trashguy instance
 * @param trashItems: The characters to trash - each one will be trashed on its own.
 * @param canGlyph: The text to use as the trashcan
 * @param leftGlyph: The text to use as the Trashguy, moving left
 * @param rightGlyph: The text to use as the Trashguy, moving right
 * @param spaceGlyph: The text to use to padding for when an element has been trashed
 * @param spacing: The number of spaces to the right of the trashguy in the first frame
 * @param trashSpaces: If set to false, any glyph that is a space (according to [CharSequence.isBlank]) will not
 * be trashed. Setting this to true improves performance significantly.
 */
fun Trashguy(
        trashItems: CharSequence,
        canGlyph: String,
        leftGlyph: String,
        rightGlyph: String,
        spaceGlyph: String,
        spacing: Int,
        trashSpaces: Boolean = true
): Trashguy {
    if (!trashSpaces)
        return Trashguy(
                trashItems.map { it.toString() },
                canGlyph,
                leftGlyph,
                rightGlyph,
                spaceGlyph,
                spacing,
                trashSpaces
        )
    return SingleCharGlyphsTrashguy(trashItems, canGlyph, leftGlyph, rightGlyph, spaceGlyph, spacing)
}

/**
 * Create a new multi-char glyphs Trashguy instance
 * @param trashItems: The characters to trash - each one will be trashed on its own.
 * @param canGlyph: The text to use as the trashcan
 * @param leftGlyph: The text to use as the Trashguy, moving left
 * @param rightGlyph: The text to use as the Trashguy, moving right
 * @param spaceGlyph: The text to use to padding for when an element has been trashed
 * @param spacing: The number of spaces to the right of the Trashguy in the first frame
 * @param trashSpaces: If set to `false`, any glyph that is a space (according to [CharSequence.isBlank]) will not
 * be trashed.
 */
fun Trashguy(
        trashItems: List<CharSequence>,
        canGlyph: String,
        leftGlyph: String,
        rightGlyph: String,
        spaceGlyph: String,
        spacing: Int,
        trashSpaces: Boolean = true
): Trashguy {
    return WideStringGlyphsTrashguy(trashItems, canGlyph, leftGlyph, rightGlyph, spaceGlyph, spacing, trashSpaces)
}

/**
 * Calculate turning point given offset and max
 * @param offset result of [calcMoved]
 * @param max result of [calcMoved] with index one greater than used for [offset]
 * @return frame index where the trashguy is at the very right hand side,
 * between the frame indices passed to [calcMoved] in [offset] and [max]
 */
internal fun calcTurningPoint(offset: Int, max: Int) = (offset + max) / 2

/**
 * Calculate frame index given moved and spacing parameters
 * @param moved the number of characters that have been moved by the trashguy
 * @param spacing the number of spaces in the first frame
 * @param spacingIndex the index offset resulting from the spacing, calculated by (spacing + 1) squared
 * @return the frame index at which this number of moved characters is first reached
 */
internal fun calcIndex(moved: Int, spacing: Int, spacingIndex: Int) = (moved + spacing).let { it * it } - spacingIndex

/**
 * Undo operation of [calcIndex]
 * @param index the frame index
 * @param spacing the number of spaces in the first frame
 * @param spacingIndex the index offset resulting from the spacing, calculated by (spacing + 1) squared
 * @return the number of characters that have been moved by the trashguy at this index
 */
internal fun calcMoved(index: Int, spacing: Int, spacingIndex: Int): Int = (sqrt((index + spacingIndex).toDouble())).toInt() - spacing
