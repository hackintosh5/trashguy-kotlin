/*
 *     Kotlin Trashguy Library
 *     Copyright (C) 2021 Hackintosh Five
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License and General Lesser Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import dev.hack5.trashguy.Trashguy
import kotlin.test.Test
import kotlin.test.assertEquals

class SingleCharGlyphsTest {
    @Test
    fun testSingleCharGlyphs() {
        val tg = Trashguy("trash", "can", "<(^_^ <)", "(> ^_^)>", " ", 2)
        assertEquals(listOf(
                "can(> ^_^)>  trash",
                "can (> ^_^)> trash",
                "can  (> ^_^)>trash",
                "can  t<(^_^ <)rash",
                "can t<(^_^ <) rash",
                "cant<(^_^ <)  rash",
                "can<(^_^ <)   rash",
                "can(> ^_^)>   rash",
                "can (> ^_^)>  rash",
                "can  (> ^_^)> rash",
                "can   (> ^_^)>rash",
                "can   r<(^_^ <)ash",
                "can  r<(^_^ <) ash",
                "can r<(^_^ <)  ash",
                "canr<(^_^ <)   ash",
                "can<(^_^ <)    ash",
                "can(> ^_^)>    ash",
                "can (> ^_^)>   ash",
                "can  (> ^_^)>  ash",
                "can   (> ^_^)> ash",
                "can    (> ^_^)>ash",
                "can    a<(^_^ <)sh",
                "can   a<(^_^ <) sh",
                "can  a<(^_^ <)  sh",
                "can a<(^_^ <)   sh",
                "cana<(^_^ <)    sh",
                "can<(^_^ <)     sh",
                "can(> ^_^)>     sh",
                "can (> ^_^)>    sh",
                "can  (> ^_^)>   sh",
                "can   (> ^_^)>  sh",
                "can    (> ^_^)> sh",
                "can     (> ^_^)>sh",
                "can     s<(^_^ <)h",
                "can    s<(^_^ <) h",
                "can   s<(^_^ <)  h",
                "can  s<(^_^ <)   h",
                "can s<(^_^ <)    h",
                "cans<(^_^ <)     h",
                "can<(^_^ <)      h",
                "can(> ^_^)>      h",
                "can (> ^_^)>     h",
                "can  (> ^_^)>    h",
                "can   (> ^_^)>   h",
                "can    (> ^_^)>  h",
                "can     (> ^_^)> h",
                "can      (> ^_^)>h",
                "can      h<(^_^ <)",
                "can     h<(^_^ <) ",
                "can    h<(^_^ <)  ",
                "can   h<(^_^ <)   ",
                "can  h<(^_^ <)    ",
                "can h<(^_^ <)     ",
                "canh<(^_^ <)      ",
                "can<(^_^ <)       ",
                "can(> ^_^)>       ",
        ), tg)
    }
}