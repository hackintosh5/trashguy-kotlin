/*
 *     Kotlin Trashguy Library
 *     Copyright (C) 2021 Hackintosh Five
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License and General Lesser Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import dev.hack5.trashguy.CustomLengthString
import dev.hack5.trashguy.Trashguy
import kotlin.test.Test
import kotlin.test.assertEquals

class WideStringGlyphsTest {
    @Test
    fun testWideStringGlyphs() {
        val tg = Trashguy(listOf("t", "r", CustomLengthString(1, "ash"), "guy", "   ", "rocks"), "can", "<(^_^ <)", "(> ^_^)>", " ", 3, false)
        assertEquals(listOf(
                "can(> ^_^)>   trashguy   rocks",
                "can (> ^_^)>  trashguy   rocks",
                "can  (> ^_^)> trashguy   rocks",
                "can   (> ^_^)>trashguy   rocks",
                "can   t<(^_^ <)rashguy   rocks",
                "can  t<(^_^ <) rashguy   rocks",
                "can t<(^_^ <)  rashguy   rocks",
                "cant<(^_^ <)   rashguy   rocks",
                "can<(^_^ <)    rashguy   rocks",
                "can(> ^_^)>    rashguy   rocks",
                "can (> ^_^)>   rashguy   rocks",
                "can  (> ^_^)>  rashguy   rocks",
                "can   (> ^_^)> rashguy   rocks",
                "can    (> ^_^)>rashguy   rocks",
                "can    r<(^_^ <)ashguy   rocks",
                "can   r<(^_^ <) ashguy   rocks",
                "can  r<(^_^ <)  ashguy   rocks",
                "can r<(^_^ <)   ashguy   rocks",
                "canr<(^_^ <)    ashguy   rocks",
                "can<(^_^ <)     ashguy   rocks",
                "can(> ^_^)>     ashguy   rocks",
                "can (> ^_^)>    ashguy   rocks",
                "can  (> ^_^)>   ashguy   rocks",
                "can   (> ^_^)>  ashguy   rocks",
                "can    (> ^_^)> ashguy   rocks",
                "can     (> ^_^)>ashguy   rocks",
                "can     ash<(^_^ <)guy   rocks",
                "can    ash<(^_^ <) guy   rocks",
                "can   ash<(^_^ <)  guy   rocks",
                "can  ash<(^_^ <)   guy   rocks",
                "can ash<(^_^ <)    guy   rocks",
                "canash<(^_^ <)     guy   rocks",
                "can<(^_^ <)      guy   rocks",
                "can(> ^_^)>      guy   rocks",
                "can (> ^_^)>     guy   rocks",
                "can  (> ^_^)>    guy   rocks",
                "can   (> ^_^)>   guy   rocks",
                "can    (> ^_^)>  guy   rocks",
                "can     (> ^_^)> guy   rocks",
                "can      (> ^_^)>guy   rocks",
                "can      guy<(^_^ <)   rocks",
                "can     guy<(^_^ <)    rocks",
                "can    guy<(^_^ <)     rocks",
                "can   guy<(^_^ <)      rocks",
                "can  guy<(^_^ <)       rocks",
                "can guy<(^_^ <)        rocks",
                "canguy<(^_^ <)         rocks",
                "can<(^_^ <)            rocks",
                "can(> ^_^)>            rocks",
                "can (> ^_^)>           rocks",
                "can  (> ^_^)>          rocks",
                "can   (> ^_^)>         rocks",
                "can    (> ^_^)>        rocks",
                "can     (> ^_^)>       rocks",
                "can      (> ^_^)>      rocks",
                "can       (> ^_^)>     rocks",
                "can        (> ^_^)>    rocks",
                "can         (> ^_^)>   rocks",
                "can          (> ^_^)>  rocks",
                "can           (> ^_^)> rocks",
                "can            (> ^_^)>rocks",
                "can            rocks<(^_^ <)",
                "can           rocks<(^_^ <)",
                "can          rocks<(^_^ <)",
                "can         rocks<(^_^ <)",
                "can        rocks<(^_^ <)",
                "can       rocks<(^_^ <)",
                "can      rocks<(^_^ <)",
                "can     rocks<(^_^ <)",
                "can    rocks<(^_^ <)",
                "can   rocks<(^_^ <)",
                "can  rocks<(^_^ <)",
                "can rocks<(^_^ <)",
                "canrocks<(^_^ <)",
                "can<(^_^ <)",
                "can(> ^_^)>",
        ), tg.map { it.trim() })
    }
}